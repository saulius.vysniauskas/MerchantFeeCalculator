﻿namespace MerchantFeeCalculator.UnitTests
{
    using MerchantFeeCalculator.Core;
    using MerchantFeeCalculator.Core.Model;
    using System;
    using System.Collections.Generic;
    using Xunit;
   
    public class InvoiceFeeCalculatorTests
    {
        [Fact]
        public void InvoiceFeeCalculator_Calculates_Fees()
        {
            var calculator = new InvoiceFeeCalculator();
            var testData = GetTestData();

            foreach (var test in testData)
            {
                Assert.Equal(test.ExpectedInvoiceFee, calculator.Calculate(test.Transaction, test.MerchantFee));
            }            
        }

        private List<TestData> GetTestData()
        {
            return new List<TestData>
            {
                // 1st month
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 1), new Merchant("MERCHANT1", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 2), new Merchant("MERCHANT1", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 2), new Merchant("MERCHANT2", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 3), new Merchant("MERCHANT3", 0), 0.5M),
                    MerchantFee = 0,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 4), new Merchant("MERCHANT2", 0), 1000),
                    MerchantFee = 10,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 5), new Merchant("MERCHANT3", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 1, 5), new Merchant("MERCHANT4", 0), 0.5M),
                    MerchantFee = 0,
                    ExpectedInvoiceFee = 0
                },
                // 2nd month
                new TestData
                {
                    Transaction =  new Transaction(new DateTime(2020, 2, 1), new Merchant("MERCHANT2", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 10), new Merchant("MERCHANT1", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction =  new Transaction(new DateTime(2020, 2, 13), new Merchant("MERCHANT3", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 15), new Merchant("MERCHANT4", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 29
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 16), new Merchant("MERCHANT1", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 16), new Merchant("MERCHANT2", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 16), new Merchant("MERCHANT3", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 0
                },
                new TestData
                {
                    Transaction = new Transaction(new DateTime(2020, 2, 16), new Merchant("MERCHANT4", 0), 100),
                    MerchantFee = 1,
                    ExpectedInvoiceFee = 0
                }
            };
        }

        private class TestData
        {
            public Transaction Transaction { get; set; }
            public decimal MerchantFee { get; set; }

            public decimal ExpectedInvoiceFee { get; set; }
        }
    }
}
