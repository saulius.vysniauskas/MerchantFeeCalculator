namespace MerchantFeeCalculator.UnitTests
{
    using MerchantFeeCalculator.Core;
    using MerchantFeeCalculator.Core.Model;
    using System;
    using Xunit;

    public class DefaultMerchantFeeCalculatorTests
    {
        [Theory, MemberData(nameof(TransactionData))]
        public void MerchantFeeCalculator_Calculates_Fees(Transaction transaction, decimal expectedResult)
        {
            var calculator = new DefaultMerchantFeeCalculator();

            Assert.Equal(expectedResult, calculator.Calculate(transaction));
        }

        public static TheoryData<Transaction, decimal> TransactionData { get; } = new TheoryData<Transaction, decimal>
        {
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 150),
                1.5M
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 100),
                1
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 50),
                0.5M
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 1),
                0.01M
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 0.5M),
                0.01M
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 0), 0.49M),
                0M
            },
            {
                new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 10), 120),
                1.08M
            }
        };
    }
}
