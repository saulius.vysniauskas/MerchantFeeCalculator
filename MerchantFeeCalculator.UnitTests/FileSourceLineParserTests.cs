﻿namespace MerchantFeeCalculator.UnitTests
{
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure;
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class FileSourceLineParserTests
    {
       
        [Theory, MemberData(nameof(TestData))]
        public void FileSourceLineParser_Parses_FileLine(
            string line, 
            DateTime expectedDate,
            string expectedMerchant,
            decimal expectedAmount)
        {
            var merchants = new List<Merchant>
            {
                new Merchant("7-ELEVEN", 0),
                new Merchant("TELIA", 0)
            };

            var fileSourceLineParser = new FileSourceLineParser(merchants);

            var transaction = fileSourceLineParser.Parse(line);

            Assert.Equal(expectedDate, transaction.Date);
            Assert.Equal(expectedAmount, transaction.Amount);
            Assert.Equal(expectedMerchant, transaction.Merchant.Name);
        }

        public static TheoryData<string, DateTime, string, decimal> TestData { get; } = new TheoryData<string, DateTime, string, decimal>
        {
            {
                "2018-09-01 7-ELEVEN 100",
                new DateTime(2018, 9 ,1),
                "7-ELEVEN",
                100
            },
            {
                "2018-10-07 TELIA    2000",
                new DateTime(2018, 10, 7),
                "TELIA",
                2000
            }
        };
    }
}
