﻿namespace MerchantFeeCalculator.UnitTests
{
    using FakeItEasy;
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure;
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class FileTransactionSourceTests
    {
        private IStreamReaderWrapper _streamReader;
        private IFileSourceLineParser _fileSourceLineParser;
        private FileTransactionSource _fileTransactionSource;

        public FileTransactionSourceTests()
        {
            _streamReader = A.Fake<IStreamReaderWrapper>();
            _fileSourceLineParser = A.Fake<IFileSourceLineParser>();
            _fileTransactionSource = new FileTransactionSource(_streamReader, _fileSourceLineParser);
        }

        [Fact]
        public async Task FileTransactionSource_Reads_File()
        {
            A.CallTo(() => _streamReader.ReadLineAsync()).ReturnsNextFromSequence("2018-09-01 7-ELEVEN 100", "  ", "2018-09-01 7-ELEVEN 100", null);
            A.CallTo(() => _fileSourceLineParser.Parse(A<string>._))
                .Returns(
                    new Transaction(new DateTime(2018, 9, 1),
                    new Merchant("7-ELEVEN", 0), 100));

            var firstTransaction = await _fileTransactionSource.GetNextTransaction();
            var secondTransaction = await _fileTransactionSource.GetNextTransaction();
            var thirdTransaction = await _fileTransactionSource.GetNextTransaction();

            Assert.NotNull(firstTransaction);
            Assert.NotNull(secondTransaction);
            Assert.Null(thirdTransaction);
        }
    }
}
