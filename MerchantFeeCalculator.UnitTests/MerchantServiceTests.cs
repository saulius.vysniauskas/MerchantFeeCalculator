﻿namespace MerchantFeeCalculator.UnitTests
{
    using FakeItEasy;
    using MerchantFeeCalculator.Core;
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure;
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class MerchantServiceTests
    {
        private ITransactionSource _transactionSource;
        private IMerchantFeeCalculator _merchantFeeCalculator;
        private IInvoiceFeeCalculator _invoiceFeeCalculator;
        private IExternalOutput _externalOutput;
        private IMerchantFeeService _merchantFeeService;

        public MerchantServiceTests()
        {
            _transactionSource = A.Fake<ITransactionSource>();
            _merchantFeeCalculator = A.Fake<IMerchantFeeCalculator>();
            _invoiceFeeCalculator = A.Fake<IInvoiceFeeCalculator>();
            _externalOutput = A.Fake<IExternalOutput>();

            _merchantFeeService = new MechantFeeService(
                _transactionSource,
                _merchantFeeCalculator,
                _invoiceFeeCalculator,
                _externalOutput);
        }

        [Fact]
        public async Task MerchantService_Calculates_Fees()
        {
            A.CallTo(() => _transactionSource.GetNextTransaction())
                .ReturnsNextFromSequence(new Transaction(DateTime.UtcNow, new Merchant("MERCHANT", 10), 100), null);

            A.CallTo(() => _merchantFeeCalculator.Calculate(A<Transaction>._)).Returns(1);

            A.CallTo(() => _invoiceFeeCalculator.Calculate(A<Transaction>._, 1)).Returns(29);

            var callToExternalOutput = A.CallTo(() => _externalOutput.Send(A<Transaction>._, 30));
            callToExternalOutput.DoesNothing();

            await _merchantFeeService.CalculateFees();
            callToExternalOutput.MustHaveHappenedOnceExactly();            
        }
    }
}
