﻿namespace MerchantFeeCalculator.UnitTests
{
    using FakeItEasy;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure;
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class ConsoleOutputTests
    {
        private IConsoleWrapper _console;
        private ConsoleOutput _consoleOutput;

        public ConsoleOutputTests()
        {
            _console = A.Fake<IConsoleWrapper>();
            _consoleOutput = new ConsoleOutput(_console, Merchants);
        }

        [Theory, MemberData(nameof(TestData))]
        public void ConsoleOutput_Send_CorrectOutput(Transaction transaction, decimal amount, string expectedOutput)
        {
            var callForTransaction = A.CallTo(() => _console.WriteLine(A<string>.That.IsEqualTo(expectedOutput)));
            callForTransaction.DoesNothing();

            _consoleOutput.Send(transaction, amount);
            callForTransaction.MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void ConsoleOutput_Separates_MonthsByEmptyLine()
        {
            var callForEmptyLine = A.CallTo(() => _console.WriteLine(A<string>.That.IsEqualTo(string.Empty)));
            callForEmptyLine.DoesNothing();

            var transaction = new Transaction(new DateTime(2020, 1, 15), FirstMerchant, 100);
            _consoleOutput.Send(transaction, 30.29M);
            callForEmptyLine.MustNotHaveHappened();

            transaction = new Transaction(new DateTime(2020, 2, 16), SecondMerchant, 100);
            _consoleOutput.Send(transaction, 0.011M);
            callForEmptyLine.MustHaveHappened();
        }

        public static Merchant FirstMerchant { get; } = new Merchant("VeryAmazingMerchant", 0);
        public static Merchant SecondMerchant { get; } = new Merchant("Merchant", 0);

        public static List<Merchant> Merchants { get; } = new List<Merchant>
        {
            FirstMerchant,
            SecondMerchant,
        };

        public static TheoryData<Transaction, decimal, string> TestData { get; } = new TheoryData<Transaction, decimal, string>
        { 
            {
                new Transaction(new DateTime(2020, 2, 16), SecondMerchant, 100),
                0.0119M,
                "2020-02-16 Merchant            0.01"
            },
            {
                new Transaction(new DateTime(1998, 2, 16), SecondMerchant, 100),
                0.009M,
                "1998-02-16 Merchant            0.00"
            },
            {
                new Transaction(new DateTime(2020, 1, 15), FirstMerchant, 100),
                30.29M,
                "2020-01-15 VeryAmazingMerchant 30.29"
            },
            {
                new Transaction(new DateTime(2020, 1, 15), FirstMerchant, 100),
                90M,
                "2020-01-15 VeryAmazingMerchant 90.00"
            },
            {
                new Transaction(new DateTime(2020, 1, 15), FirstMerchant, 100),
                99.96M,
                "2020-01-15 VeryAmazingMerchant 99.96"
            },
            {
                new Transaction(new DateTime(2020, 1, 16), SecondMerchant, 100),
                130.989M,
                "2020-01-16 Merchant            130.98"
            },
            {
                new Transaction(new DateTime(2020, 12, 1), FirstMerchant, 100),
                11000,
                "2020-12-01 VeryAmazingMerchant 11000.00"
            }
        };
    }
}
