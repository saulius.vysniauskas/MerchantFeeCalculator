﻿namespace MerchantFeeCalculator.Core
{
    using MerchantFeeCalculator.Core.Model;

    public class DefaultMerchantFeeCalculator : IMerchantFeeCalculator
    {
        public decimal Calculate(Transaction transaction)
        {
            var fee = transaction.Amount * 0.01M * (1 - transaction.Merchant.Discount / 100);

           return decimal.Round(fee, 2, System.MidpointRounding.AwayFromZero);
        }
    }
}
