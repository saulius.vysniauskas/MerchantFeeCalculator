﻿namespace MerchantFeeCalculator.Core
{
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure;
    using System.Threading.Tasks;

    public class MechantFeeService : IMerchantFeeService
    {
        private readonly ITransactionSource _transactionSource;
        private readonly IMerchantFeeCalculator _merchantFeeCalculator;
        private readonly IExternalOutput _output;
        private readonly IInvoiceFeeCalculator _invoiceFeeCalculator;

        public MechantFeeService(
            ITransactionSource transactionSource, 
            IMerchantFeeCalculator merchantFeeCalculator,
            IInvoiceFeeCalculator invoiceFeeCalculator,
            IExternalOutput output)
        {
            _transactionSource = transactionSource;
            _merchantFeeCalculator = merchantFeeCalculator;
            _invoiceFeeCalculator = invoiceFeeCalculator;
            _output = output;
        }

        public async Task CalculateFees()
        {
            Transaction transaction = null;

            while ((transaction = await _transactionSource.GetNextTransaction()) != null)
            {
                var merchantFee = _merchantFeeCalculator.Calculate(transaction);

                merchantFee += _invoiceFeeCalculator.Calculate(transaction, merchantFee);

                _output.Send(transaction, merchantFee);
            }
        }
    }
}