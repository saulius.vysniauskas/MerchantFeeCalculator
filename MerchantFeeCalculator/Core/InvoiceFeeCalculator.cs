﻿namespace MerchantFeeCalculator.Core
{
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using System.Collections.Generic;

    public class InvoiceFeeCalculator : IInvoiceFeeCalculator
    {
        public const decimal InvoiceFee = 29;
        private Dictionary<string, decimal> _invoiceFees;

        public InvoiceFeeCalculator()
        {
            _invoiceFees = new Dictionary<string, decimal>();
        }

        public decimal Calculate(Transaction transaction, decimal merchantFee)
        {
            var uniqueKey = $"{transaction.Date.ToString("yyyy-MM")}{transaction.Merchant.Name}";
            decimal invoiceFee;

            if (_invoiceFees.TryGetValue(uniqueKey, out invoiceFee))
            {
                _invoiceFees[uniqueKey] = 0;
                return invoiceFee;
            }

            if (merchantFee > 0)
            {
                _invoiceFees.Add(uniqueKey, 0);
                return InvoiceFee;
            }

            _invoiceFees.Add(uniqueKey, InvoiceFee);
            return 0;
        }
    }
}
