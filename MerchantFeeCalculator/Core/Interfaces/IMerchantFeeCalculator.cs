﻿namespace MerchantFeeCalculator.Core
{
    using MerchantFeeCalculator.Core.Model;

    public interface IMerchantFeeCalculator
    {
        decimal Calculate(Transaction transaction);
    }
}
