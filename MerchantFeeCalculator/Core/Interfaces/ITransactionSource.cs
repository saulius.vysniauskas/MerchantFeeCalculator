﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Model;
    using System.Threading.Tasks;

    public interface ITransactionSource
    {
        Task<Transaction> GetNextTransaction();
    }
}
