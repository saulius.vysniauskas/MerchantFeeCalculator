﻿namespace MerchantFeeCalculator.Core.Interfaces
{
    using MerchantFeeCalculator.Core.Model;

    public interface IExternalOutput
    {
        void Send(Transaction transaction, decimal merchantFee);
    }
}
