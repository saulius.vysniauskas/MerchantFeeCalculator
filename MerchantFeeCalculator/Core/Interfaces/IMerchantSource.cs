﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Model;
    using System.Collections.Generic;

    public interface IMerchantSource
    {
        IEnumerable<Merchant> GetMerchants();
    }
}
