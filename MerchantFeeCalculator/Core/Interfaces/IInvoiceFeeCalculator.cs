﻿namespace MerchantFeeCalculator.Core.Interfaces
{
    using MerchantFeeCalculator.Core.Model;

    public interface IInvoiceFeeCalculator
    {
        decimal Calculate(Transaction transaction, decimal merchantFee);
    }
}
