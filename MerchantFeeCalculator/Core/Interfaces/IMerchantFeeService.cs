﻿namespace MerchantFeeCalculator.Core
{
    using System.Threading.Tasks;

    public interface IMerchantFeeService
    {
        Task CalculateFees();
    }
}
