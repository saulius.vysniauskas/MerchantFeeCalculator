﻿namespace MerchantFeeCalculator.Core.Model
{
    using System;

    public class Transaction
    {
        public DateTime Date { get; }

        public Merchant Merchant { get; }

        public decimal Amount { get; }

        public Transaction(DateTime date, Merchant merchant, decimal amount)
        {
            Date = date;
            Merchant = merchant;
            Amount = amount;
        }
    }
}
