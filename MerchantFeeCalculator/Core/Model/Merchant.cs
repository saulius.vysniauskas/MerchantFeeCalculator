﻿namespace MerchantFeeCalculator.Core.Model
{    
    public class Merchant
    {
        public string Name { get; }

        public decimal Discount { get;  }

        public Merchant(string name, decimal discount)
        {
            Name = name;
            Discount = discount;
        }
    }
}
