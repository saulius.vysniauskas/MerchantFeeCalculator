﻿namespace MerchantFeeCalculator
{
    using MerchantFeeCalculator.Core;
    using MerchantFeeCalculator.Infrastructure;
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    class Program
    {
        static async Task Main(string[] args)
        {
            StreamReaderWrapper fileReader = null;
            try
            {
                var merchantSource = new MerchantSource();
                var merchants = merchantSource.GetMerchants().ToList();

                fileReader = new StreamReaderWrapper("transactions.txt");
                var fileSourceLineParser = new FileSourceLineParser(merchants);
                var fileSource = new FileTransactionSource(
                    fileReader,
                    fileSourceLineParser);

                var console = new ConsoleWrapper();
                var output = new ConsoleOutput(console, merchants);
                var merchantFeeCalculator = new DefaultMerchantFeeCalculator();
                var invoiceFeeCalculator = new InvoiceFeeCalculator();

                var merchantFeeService = new MechantFeeService(
                    fileSource,
                    merchantFeeCalculator,
                    invoiceFeeCalculator,
                    output);

                await merchantFeeService.CalculateFees();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error occured: {ex.Message}");
                Console.WriteLine($"Stack trace: {ex.StackTrace}");                
            }
            finally
            {
                if (fileReader != null)
                {
                    fileReader.Close();
                }
            }
        }
    }
}
