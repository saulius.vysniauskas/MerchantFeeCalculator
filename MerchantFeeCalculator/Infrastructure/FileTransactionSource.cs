﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System.Threading.Tasks;

    public class FileTransactionSource : ITransactionSource
    {
        private readonly IStreamReaderWrapper _fileReader;
        private readonly IFileSourceLineParser _fileSourceLineParser;

        public FileTransactionSource(
            IStreamReaderWrapper fileReader,
            IFileSourceLineParser fileSourceLineParser)
        {
            _fileReader = fileReader;
            _fileSourceLineParser = fileSourceLineParser;
        }

        public async Task<Transaction> GetNextTransaction()
        {
            string transactionLine;

            while ((transactionLine = await _fileReader.ReadLineAsync()) != null)
            {
                if (transactionLine.Trim() == string.Empty)
                {
                    continue;
                }

                return _fileSourceLineParser.Parse(transactionLine);
            }

            return null;
        }
    }
}
