﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class ConsoleOutput : IExternalOutput
    {
        private IConsoleWrapper _console;
        private readonly int _maxMerchantNameLenght;
        private string _previousMonth;

        public ConsoleOutput(IConsoleWrapper console, List<Merchant> merchants)
        {
            _maxMerchantNameLenght = merchants.Max(m => m.Name.Length);
            _console = console;
        }

        public void Send(Transaction transaction, decimal merchantFee)
        {
            var thisMonth = transaction.Date.ToString("yyyyMM");
            if (_previousMonth != null && _previousMonth != thisMonth)
            {
                _console.WriteLine();
            }

            merchantFee = Math.Truncate(merchantFee * 100) / 100;

            _console.WriteLine($"{transaction.Date:yyyy-MM-dd} " +
                $"{transaction.Merchant.Name.PadRight(_maxMerchantNameLenght)} " +
                $"{merchantFee.ToString("0.00#", CultureInfo.InvariantCulture)}");
            
            _previousMonth = thisMonth;
        }
    }
}
