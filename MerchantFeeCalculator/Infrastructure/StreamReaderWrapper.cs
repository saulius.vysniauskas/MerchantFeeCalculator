﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System.IO;
    using System.Threading.Tasks;

    public class StreamReaderWrapper : IStreamReaderWrapper
    {
        private StreamReader _streamReader;

        public StreamReaderWrapper(string filePath)
        {
            _streamReader = new StreamReader(filePath); 
        }

        public Task<string> ReadLineAsync()
        {
            return _streamReader.ReadLineAsync();
        }

        public void Close()
        {
            _streamReader.Close();
        }
    }
}
