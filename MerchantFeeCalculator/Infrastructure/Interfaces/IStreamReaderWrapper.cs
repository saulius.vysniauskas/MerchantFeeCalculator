﻿namespace MerchantFeeCalculator.Infrastructure.Interfaces
{
    using System.Threading.Tasks;

    public interface IStreamReaderWrapper
    {
        Task<string> ReadLineAsync();

        public void Close();
    }
}
