﻿namespace MerchantFeeCalculator.Core.Interfaces
{
    using MerchantFeeCalculator.Core.Model;

    public interface IFileSourceLineParser
    {
        Transaction Parse(string line);
    }
}
