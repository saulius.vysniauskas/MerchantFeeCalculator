﻿namespace MerchantFeeCalculator.Infrastructure.Interfaces
{
    public interface IConsoleWrapper
    {
        void WriteLine(string line = "");
    }
}
