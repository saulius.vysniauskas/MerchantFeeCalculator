﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Interfaces;
    using MerchantFeeCalculator.Core.Model;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public class FileSourceLineParser : IFileSourceLineParser
    {
        private readonly List<Merchant> _merchants;

        public FileSourceLineParser(List<Merchant> merchants)
        {
            _merchants = merchants;
        }

        public Transaction Parse(string line)
        {
            var parsedData = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var date = Convert.ToDateTime(parsedData[0]);

            var merchant = _merchants.Single(m => m.Name == parsedData[1]);

            var amount = Convert.ToDecimal(parsedData[2], CultureInfo.InvariantCulture);

            return new Transaction(date, merchant, amount);
        }
    }
}
