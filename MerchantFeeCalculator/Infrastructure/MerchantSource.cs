﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Core.Model;
    using System.Collections.Generic;

    public class MerchantSource : IMerchantSource
    {
        public IEnumerable<Merchant> GetMerchants()
        {
            return new List<Merchant>
            {
                new Merchant("TELIA", 10),
                new Merchant("CIRCLE_K", 20),
                new Merchant("NETTO", 0),
                new Merchant("7-ELEVEN", 0)
            };
        }
    }
}
