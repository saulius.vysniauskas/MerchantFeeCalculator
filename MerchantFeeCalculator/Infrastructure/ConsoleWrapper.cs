﻿namespace MerchantFeeCalculator.Infrastructure
{
    using MerchantFeeCalculator.Infrastructure.Interfaces;
    using System;

    public class ConsoleWrapper : IConsoleWrapper
    {
        public void WriteLine(string line = "")
        {
            Console.WriteLine(line);
        }
    }
}
